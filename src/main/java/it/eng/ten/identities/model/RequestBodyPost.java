package it.eng.ten.identities.model;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author ascatox
 * @since 04/02/22
 */
public class RequestBodyPost {
    @ApiModelProperty(example = "1K31KZXjcochXpRhjH9g5MxFFTHPi2zEXb")
    private String cid;
    @ApiModelProperty(value = "Ext Data", example = "\"{interq-idsc-org:{\"LegalName\":\"ACME Corporation\"}}\"")
    private String ext;
    @ApiModelProperty(value = "kType", example = "ECDSA")
    @NotBlank(message = "The kType is a required field")
    @Pattern(regexp = "ECDSA", message = "The only value allowed is ECDSA")
    private String kType;
    @NotBlank(message = "The pubKey is a required field")
    @ApiModelProperty(example = "BDefACis5x40248TtHxon2r8FRIQJRYeTrR1S7Ub+z5GVh5oXOb4YiRvNF0S8VZtKkdxSwq0Y+ZYnjbnvw7ZRqU=")
    private String pubKey;
    @ApiModelProperty(example = "mysecret")
    @NotBlank(message = "The pwd is a required field")
    private String pwd;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getkType() {
        return kType;
    }

    public void setkType(String kType) {
        this.kType = kType;
    }

    public String getPubKey() {
        return pubKey;
    }

    public void setPubKey(String pubKey) {
        this.pubKey = pubKey;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
