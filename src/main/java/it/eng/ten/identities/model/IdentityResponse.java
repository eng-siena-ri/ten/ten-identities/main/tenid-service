package it.eng.ten.identities.model;

/**
 * @author clod16
 * @project tenid-service
 * @date 04/02/2022
 */
public class IdentityResponse {
    private String id;
    private String pKey;
    private String kType;
    private String end;
    private String status;
    private String ext;

    public IdentityResponse() {
    }

    public IdentityResponse(String id, String pKey, String kType, String end, String status, String ext) {
        this.id = id;
        this.pKey = pKey;
        this.kType = kType;
        this.end = end;
        this.status = status;
        this.ext = ext;
    }

    @Override
    public String toString() {
        return "IdentityResponse{" +
                "id='" + id + '\'' +
                ", pKey='" + pKey + '\'' +
                ", kType='" + kType + '\'' +
                ", end='" + end + '\'' +
                ", status='" + status + '\'' +
                ", ext='" + ext + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getpKey() {
        return pKey;
    }

    public void setpKey(String pKey) {
        this.pKey = pKey;
    }

    public String getkType() {
        return kType;
    }

    public void setkType(String kType) {
        this.kType = kType;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}
