package it.eng.ten.identities.model;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author ascatox
 * @since 04/02/22
 */
public class RequestBodyPut {
    @ApiModelProperty(example = "1K31KZXjcochXpRhjH9g5MxFFTHPi2zEXb")
    @NotBlank(message = "The cid is a required field")
    private String cid;
    @ApiModelProperty(value = "Operation", example = "S|R|A")
    @NotBlank(message = "The op is a required field")
    @Pattern(regexp = "S|R|A", message = "The possible op are 'S' or 'R' or 'A'")
    private String op;
    @ApiModelProperty(example = "mysecret")
    @NotBlank(message = "The pwd is a required field")
    private String pwd;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
