package it.eng.ten.identities.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.eng.ten.identities.chaincode.FunctionStatus;
import it.eng.ten.identities.chaincode.HttpStatusCode;
import it.eng.ten.identities.lib.TenIdentitiesService;
import it.eng.ten.identities.lib.config.Config;
import it.eng.ten.identities.lib.model.Response;
import it.eng.ten.identities.lib.model.crypto.ECDSApublicKeyXY;
import it.eng.ten.identities.lib.model.identity.Identity;
import it.eng.ten.identities.lib.model.identity.IdentityBase;
import it.eng.ten.identities.lib.model.identity.IdentityStatus;
import it.eng.ten.identities.lib.utils.ECDSA;
import it.eng.ten.identities.lib.utils.JsonHandler;
import it.eng.ten.identities.lib.utils.MethodUtils;
import it.eng.ten.identities.model.IdentityResponse;
import it.eng.ten.identities.model.RequestBodyPost;
import it.eng.ten.identities.model.RequestBodyPut;
import org.apache.commons.lang3.StringUtils;
import org.hyperledger.fabric.gateway.GatewayException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.ServletContext;
import javax.validation.Valid;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.logging.Logger;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


/**
 * @author clod16
 * @project ten-identities-rest-api
 * @date 03/11/2021
 */

@RestController
@RequestMapping(value = "/v1.0/ids")
@Api(value = "", tags = "TEnID Service API")
public class TenIdentitiesRestControllerLib {


    @Value("${enable.tomcat}")
    private String enableTomcat;


    private static final Logger log = Logger.getLogger(TenIdentitiesRestControllerLib.class.getName());
    public static final String NOT_FOUND = "Not Found";
    public static final String UNAUTHORIZED = "Unauthorized";
    public static final String INTERNAL = "Internal server error";
    public static final String BAD_REQUEST = "Bad Request";
    public static final String BAD_GATEWAY = "Bad Gateway";
    public static final String UNPROCESSABLE_ENTITY = "Unprocessable Entity";

    public static final String FORBIDDEN = "Forbidden";
    public static final String CREATED = "Created";
    public static final String OK = "Ok";
    public static final String CONFLICT = "Conflict";
    @Autowired
    private ServletContext servletContext;

    @Autowired
    private Environment env;

    @GetMapping(value = "/{id}")
    @ApiOperation("Returns an Identity.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = OK),
            @ApiResponse(code = 404, message = NOT_FOUND),
            @ApiResponse(code = 401, message = UNAUTHORIZED),
            @ApiResponse(code = 502, message = BAD_GATEWAY),
            @ApiResponse(code = 500, message = INTERNAL),
    })
    public IdentityResponse getIdentity(@PathVariable(name = "id") String id,
                                        @RequestParam(name = "pwd") String pwd,
                                        @RequestParam(name = "cid") String cid) throws Exception {
        log.info("getIdentity API starting....");
        Config config = getConfig();
        it.eng.ten.identities.lib.TenIdentitiesService identitiesService =
                new it.eng.ten.identities.lib.TenIdentitiesServiceImpl(config);
        Response response1 = null;
        try {
            response1 = identitiesService.submitTransaction(cid, pwd, "getIdentity", cid + "@" + id);
        } catch (GatewayException e) {
            Response response = getResponseExceptionLib(e);
            throw returnResponseStatusException(response);
        }
        if (response1 == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
        Identity identity;
        if (response1.getHeader().getCode() != HttpStatusCode.NOT_FOUND.getCode()) {
            try {
                Base64.Encoder encoder = Base64.getEncoder();
                identity = (Identity) JsonHandler.convertFromJson(response1.getMessage(), Identity.class);
                final PublicKey ecPublicKey = ECDSA.hexTOPublicKey(identity.getIdentityBase().getpKey());
                identity.getIdentityBase().setpKey(encoder.encodeToString(ecPublicKey.getEncoded()));
                String status = "";
                if (identity.getIdentityStatus().getStatus() == 1) {
                    status = "A";
                } else if (identity.getIdentityStatus().getStatus() == 2) {
                    status = "S";

                } else if (identity.getIdentityStatus().getStatus() == 3) {
                    status = "R";
                }
                return new IdentityResponse(identity.getIdentityBase().getId(), identity.getIdentityBase().getpKey(),
                        identity.getIdentityBase().getkType(), identity.getIdentityBase().getEnd(), status,
                        identity.getIdentityBase().getExt());
            } catch (Exception e) {
                log.severe(e.getMessage());
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Identity not found");
    }

    public Identity postIdentityRoot(String pubKey, String password) {
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(decoder.decode(pubKey)));
            Identity identity = new Identity();
            IdentityStatus identityStatus = new IdentityStatus();
            identity.setIdentityStatus(identityStatus);
            IdentityBase identityBase = new IdentityBase();
            ECDSApublicKeyXY ecdsApublicKeyXY = ECDSA.getPublicKeyAsHex(publicKey);
            identityBase.setpKey(JsonHandler.convertToJson(ecdsApublicKeyXY));
            identityBase.setEnd("");
            identityBase.setExt("ROOT");
            identityBase.setkType("ECDSA");
            identity.setIdentityBase(identityBase);
            Config config = getConfig();
            it.eng.ten.identities.lib.TenIdentitiesService identitiesService =
                    new it.eng.ten.identities.lib.TenIdentitiesServiceImpl(config);
            it.eng.ten.identities.lib.model.Response response = identitiesService.submitTransaction("", password,
                    "postIdentityRoot", JsonHandler.convertToJson(identity));

            if (null == response || null == response.getMessage()) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Response is empty");
            }
            Identity identity2 = (Identity) JsonHandler.convertFromJson(response.getMessage(), Identity.class);
            return identity2;
        } catch (Exception e) {
            log.severe(e.getMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());

        }
    }


    @PostMapping(path = "", consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.CREATED)
    @ApiOperation("Creates an Identity. ")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = CREATED, response = Identity.class),
            @ApiResponse(code = 401, message = UNAUTHORIZED),
            @ApiResponse(code = 400, message = BAD_REQUEST),
            @ApiResponse(code = 422, message = UNPROCESSABLE_ENTITY),
            @ApiResponse(code = 502, message = BAD_GATEWAY),
            @ApiResponse(code = 409, message = CONFLICT),
            @ApiResponse(code = 500, message = INTERNAL),
    })
    public String postIdentity(@Valid @RequestBody RequestBodyPost requestBodyPost) throws Exception {

        String pubKey = requestBodyPost.getPubKey();
        String pwd = requestBodyPost.getPwd();
        String kType = requestBodyPost.getkType();
        String ext = requestBodyPost.getExt();
        String cid = requestBodyPost.getCid();

        if (StringUtils.isEmpty(requestBodyPost.getCid())) {
            try {
                Identity root = postIdentityRoot(pubKey, pwd);
                return root.getIdentityBase().getId();
            } catch (Exception e) {
                log.severe(e.getMessage());
                throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, e.getMessage());
            }
        }
        Identity identity = new Identity();
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            Base64.Decoder decoder = Base64.getDecoder();
            IdentityStatus identityStatus = new IdentityStatus();
            identity.setIdentityStatus(identityStatus);
            IdentityBase identityBase = new IdentityBase();
            ECDSApublicKeyXY ecdsApublicKeyXY;
            try {
                PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(decoder.decode(pubKey)));
                ecdsApublicKeyXY = ECDSA.getPublicKeyAsHex(publicKey);
            } catch (Exception e) {
                log.severe(e.getMessage());
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "PublicKey bad format error");
            }
            identityBase.setpKey(JsonHandler.convertToJson(ecdsApublicKeyXY));
            identityBase.setEnd(cid);
            if (ext != null) {
                identityBase.setExt(ext);

            } else {
                identityBase.setExt("");

            }
            if (kType.equals("ECDSA") || kType.equals("ecdsa")) {
                identityBase.setkType("ECDSA");
            } else {
                log.severe("Error in KType value");
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, " Error in KType value");
            }
            identity.setIdentityBase(identityBase);
        } catch (Exception e) {
            log.severe(e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        Config config = getConfig();
        Response response = null;
        try {
            it.eng.ten.identities.lib.TenIdentitiesService identitiesService =
                    new it.eng.ten.identities.lib.TenIdentitiesServiceImpl(config);
            response = identitiesService.submitTransaction(cid, pwd, "postIdentity",
                    JsonHandler.convertToJson(identity));
        } catch (GatewayException e) {
            response = getResponseExceptionLib(e);
            throw returnResponseStatusException(response);
        }

        Identity identityReturn = null;
        try {
            identityReturn = (Identity) JsonHandler.convertFromJson(response.getMessage(), Identity.class);
        } catch (Exception e) {
            log.severe(response.getMessage());
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, e.getMessage());
        }
        return identityReturn.getIdentityBase().getId();
    }


    private Response getResponseExceptionLib(GatewayException e) {
        try {
            return (Response) JsonHandler.convertFromJson(MethodUtils.removeExceptionClassFromMsg(e), Response.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //Not used now, but present in Chaincode
    /*
    @PutMapping(path = "/{id}/ext", consumes = APPLICATION_FORM_URLENCODED_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @ApiOperation("Changes the 'ext' value of an Identity.")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "No Content"),
                           @ApiResponse(code = 401, message = UNAUTHORIZED),
                           @ApiResponse(code = 404, message = NOT_FOUND),})
    public void editExtIdentity(@PathVariable(name = "id") String id,
                                @RequestParam(name = "ext") String ext,
                                @RequestParam(name = "pwd") String pwd,
                                @RequestParam(name = "cid") String cid) {
        if (StringUtils.isNotEmpty(ext)) {
            try {
                log.info(cid + " API starting....");
                Response response = doEdit(id, pwd, Function.editIdentity.name(), ext, cid);
                if (response == null) {
                    throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Generic error in chaincode api");
                } else {
                    if (response.getHeader().getCode() == HttpStatusCode.CONFLICT.getCode()) {
                        log.severe(response.getMessage());
                        throw new ResponseStatusException(HttpStatus.CONFLICT, "Identity already present");
                    } else if (response.getHeader().getCode() == HttpStatusCode.NOT_FOUND.getCode()) {
                        log.severe(response.getMessage());
                        throw new ResponseStatusException(HttpStatus.NOT_FOUND, response.getHeader().getDescription());
                    } else if (response.getHeader().getCode() == HttpStatusCode.UNAUTHORIZED.getCode()) {
                        log.severe(response.getMessage());
                        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, response.getMessage());
                    } else if (response.getHeader().getCode() == HttpStatusCode.BAD_REQUEST.getCode()) {
                        log.severe(response.getMessage());
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, response.getHeader().getDescription
                        ());
                    }
                }
            } catch (Exception e) {
                log.severe(e.getMessage());
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
            }
        } else {
            log.severe("Ext Parameter is mandatory!");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Ext Parameter is mandatory");
        }
    }
     */

    @PutMapping(path = "/{id}", consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @ApiOperation("Activates, Suspends or Revokes an Identity. ")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "No Content"),
            @ApiResponse(code = 401, message = UNAUTHORIZED),
            @ApiResponse(code = 400, message = BAD_REQUEST),
            @ApiResponse(code = 422, message = UNPROCESSABLE_ENTITY),
            @ApiResponse(code = 502, message = BAD_GATEWAY),
            @ApiResponse(code = 409, message = CONFLICT),
            @ApiResponse(code = 500, message = INTERNAL),
    })
    public void editIdentity(@PathVariable(name = "id") String id, @Valid @RequestBody RequestBodyPut requestBodyPut) throws Exception {
        String op = requestBodyPut.getOp();
        String password = requestBodyPut.getPwd();
        String cid = requestBodyPut.getCid();

        if (FunctionStatus.R.name().equals(op) || FunctionStatus.A.name().equals(op) || FunctionStatus.S.name().equals(op)) {
            Response response = null;
            try {
                FunctionStatus opFcn = FunctionStatus.valueOf(op);
                log.info(op + " API starting....");
                response = doEdit(id, password, opFcn.getDescription(), "", cid);
            } catch (GatewayException e) {
                response = getResponseExceptionLib(e);
                throw returnResponseStatusException(response);
            }
        } else {
            log.severe("Bad operation name in  PUT API!");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad operation name");
        }
    }


    private ResponseStatusException returnResponseStatusException(Response response) {
        String description = response.getHeader().getDescription();
        if (response.getHeader().getCode() == HttpStatusCode.CONFLICT.getCode()) {
            log.severe(description);
            return new ResponseStatusException(HttpStatus.CONFLICT, description);
        } else if (response.getHeader().getCode() == HttpStatusCode.NOT_FOUND.getCode()) {
            log.severe(description);
            return new ResponseStatusException(HttpStatus.NOT_FOUND, description);
        } else if (response.getHeader().getCode() == HttpStatusCode.UNAUTHORIZED.getCode()) {
            log.severe(response.getMessage());
            return new ResponseStatusException(HttpStatus.UNAUTHORIZED, response.getMessage());
        } else if (response.getHeader().getCode() == HttpStatusCode.BAD_REQUEST.getCode()) {
            log.severe(description);
            return new ResponseStatusException(HttpStatus.BAD_REQUEST, description);
        } else if (response.getHeader().getCode() == HttpStatusCode.INTERNAL_SERVER_ERROR.getCode()) {
            log.severe(response.getMessage());
            return new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, response.getHeader().getDescription());
        } else if (response.getHeader().getCode() == HttpStatusCode.UNPROCESSABLE_ENTITY.getCode()) {
            log.severe(response.getMessage());
            return new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, response.getHeader().getDescription());
        }
        return new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, response.getHeader().getDescription());
    }

    private it.eng.ten.identities.lib.model.Response doEdit(String id, String password, String functionName,
                                                            String ext, String controllerAddress) throws Exception {

        Identity identity = new Identity();
        IdentityStatus identityStatus = new IdentityStatus();
        IdentityBase identityBase = new IdentityBase();
        identityBase.setId(id);
        identityBase.setEnd(controllerAddress);
        identity.setIdentityBase(identityBase);
        identity.setIdentityStatus(identityStatus);
        identityBase.setExt(ext);

        Config config = getConfig();
        TenIdentitiesService identitiesService = new it.eng.ten.identities.lib.TenIdentitiesServiceImpl(config);

        switch (functionName) {
            case "revokeIdentity":
                return identitiesService.submitTransaction(controllerAddress, password, "revokeIdentity",
                        JsonHandler.convertToJson(identity));
            case "activateIdentity":
                return identitiesService.submitTransaction(controllerAddress, password, "activateIdentity",
                        JsonHandler.convertToJson(identity));
            case "suspendIdentity":
                return identitiesService.submitTransaction(controllerAddress, password, "suspendIdentity",
                        JsonHandler.convertToJson(identity));
            case "editIdentity":
                return identitiesService.submitTransaction(controllerAddress, password, "editIdentity",
                        JsonHandler.convertToJson(identity));
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error in operation name");
    }


    private Config getConfig() {

        String organizationMspId;
        String channelName;
        String userName;
        String chaincodeName;
        String networkFilename;
        String walletIdentitiesPath;
        String walletFabricPath;
        if (enableTomcat.equals("true")) {

            organizationMspId = servletContext.getInitParameter("organizationMspId");
            channelName = servletContext.getInitParameter("channelName");
            userName = servletContext.getInitParameter("userName");
            chaincodeName = servletContext.getInitParameter("chaincodeName");
            networkFilename = servletContext.getInitParameter("networkFilename");
            walletIdentitiesPath = servletContext.getInitParameter("walletIdentitiesPath");
            walletFabricPath = servletContext.getInitParameter("walletFabricPath");

        } else {

            organizationMspId = env.getProperty("fabric.organizationMspId");
            channelName = env.getProperty("fabric.channelName");
            userName = env.getProperty("fabric.userName");
            chaincodeName = env.getProperty("fabric.chaincodeName");
            networkFilename = env.getProperty("fabric.networkFilename");
            walletIdentitiesPath = env.getProperty("fabric.walletIdentitiesPath");
            walletFabricPath = env.getProperty("fabric.walletFabricPath");

        }
        return new Config(organizationMspId, channelName, userName, chaincodeName, networkFilename,
                walletIdentitiesPath, walletFabricPath);
    }

    public static String checkExceptionCause(Exception e) {
        if (null != e.getCause()) {
            return e.getCause().getMessage();
        } else {
            return e.getMessage();
        }
    }


}


