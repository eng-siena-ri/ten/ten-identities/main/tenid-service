package it.eng.ten.identities.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Component
@ConfigurationProperties(prefix = "fabric")
public class HLFConfigPropertiesBean {


    private String organizationMspId;
    private String channelName;
    private String userName;
    private String chaincodeNameAim;
    private String chaincodeNameRole;
    private String chaincodeNameSid;
    private String networkFilename;


    public String getOrganizationMspId() {
        return organizationMspId;
    }

    public void setOrganizationMspId(String organizationMspId) {
        this.organizationMspId = organizationMspId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getChaincodeNameAim() {
        return chaincodeNameAim;
    }

    public void setChaincodeNameAim(String chaincodeNameAim) {
        this.chaincodeNameAim = chaincodeNameAim;
    }

    public String getChaincodeNameRole() {
        return chaincodeNameRole;
    }

    public void setChaincodeNameRole(String chaincodeNameRole) {
        this.chaincodeNameRole = chaincodeNameRole;
    }

    public String getChaincodeNameSid() {
        return chaincodeNameSid;
    }

    public void setChaincodeNameSid(String chaincodeNameSid) {
        this.chaincodeNameSid = chaincodeNameSid;
    }

    public String getNetworkFilename() {
        return networkFilename;
    }

    public void setNetworkFilename(String networkFilename) {
        this.networkFilename = networkFilename;
    }

//    public void setWalletPath(String walletPath) {
//        HLFConfigPropertiesBean.WALLET_PATH = walletPath;
//    }


}
