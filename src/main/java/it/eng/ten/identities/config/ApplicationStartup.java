package it.eng.ten.identities.config;

import net.lingala.zip4j.ZipFile;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Logger;

/**
 * @author Antonio Scatoloni on 04/09/2020
 **/

@Component
public class ApplicationStartup {

    private static final Logger log = Logger.getLogger(ApplicationStartup.class.getName());
    public static  String MAIN_DIRECTORY = ""; //TODO Not the BEST SOLUTION IMHO!!!
    public static  String WALLET = "";
    public static  String WALLET_PATH = "";
    public static  String WALLET_PATH_ZIP = "";
    public static  String ENV_NAME = "WALLET_PATH";
    public static  final String WALLET_TEN_NAME = "identities-wallet";


}