package it.eng.ten.identities;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.nio.file.Path;
import java.time.LocalDate;
import java.util.logging.Logger;

@SpringBootApplication
public class TenTokenRestApiApplication extends SpringBootServletInitializer {
    private static final Logger log = Logger.getLogger(TenTokenRestApiApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(TenTokenRestApiApplication.class, args);
        log.info("Server started at : localhost:8280/swagger-ui.html ");

    }


}
