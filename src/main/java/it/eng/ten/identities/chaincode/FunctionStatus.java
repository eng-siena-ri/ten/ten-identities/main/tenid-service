package it.eng.ten.identities.chaincode;

/**
 * @author Antonio Scatoloni on 22/02/2021
 **/

public enum FunctionStatus {
    S("suspendIdentity"),
    R("revokeIdentity"),
    A("activateIdentity");

    private String description;

    FunctionStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
