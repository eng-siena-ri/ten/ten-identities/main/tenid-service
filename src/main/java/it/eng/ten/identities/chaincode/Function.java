package it.eng.ten.identities.chaincode;

public enum Function {

    postIdentity,
    suspendIdentity,
    revokeIdentity,
    activateIdentity,
    getIdentity,
    getAllIdentities,
    getIdentitiesFiltered,
    getHierarchyIdentity,
    editIdentity,
    postAim,
    suspendAim,
    revokeAim,
    activateAim,
    getAim,
    ;
}
