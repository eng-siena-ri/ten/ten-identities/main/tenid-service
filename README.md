# Identity Management API
## Configuration

The **.war application** provided which needs to be deployed on a tomcat 8.x server. The rest api has inside the ten-identities-lib which must be configured with some parameters (see the ten-identities-lib documentation for further information), these parameters are read from the tomcat context.xml file. It is therefore necessary to add them completely to the configuration file, you can see an example below.
```
<Context>
 <WatchedResource>WEB-INF/web.xml</WatchedResource>
 <WatchedResource>${catalina.base}/conf/web.xml</WatchedResource>  
 
<Parameter name="chaincodeName" value="tenIdentities" override="false"/>
<Parameter name="channelName" value="mychannel" override="false"/>
<Parameter name="organizationMspId" value="Org1MSP" override="false"/>
<Parameter name="userName" value="admin" override="false"/>
<Parameter name="networkFilename" value="connection-org1.json" override="false"/>
<Parameter name="walletIdentitiesPath" value="/Users/walletIdentities" override="false"/>
<Parameter name="walletFabricPath" value="/Users/wallet" override="false"/>
</Context>
```

The application uses [Swagger OpenApi](https://swagger.io/), accessible at the path *(endpoint address)*/*swagger-ui.
html#*

## Usage


In this section we provide the documentation of the OpenAPI calls
related to identity management. All calls are rooted at the
**/v1.0/ids** path. The same general consideration made for the
Quality Hallmark API (versioning, access control parameters and
parameter value encoding) also apply here. On the contrary, identities
are *not* subject to scoping: one single identity registry is
in place that covers all scopes.

### Register a new item

Allows the caller to register a new identity, which will be created in
the Active status. The caller *must* provide the public key
assigned to the identity; the key which must be unique in the registry.
Optionally, the caller may also provide some extended information (e.g.,
describing the real-world entity that owns the identity), which will
also be assigned to the identity. Unless the caller has special
administrative privileges[^1], the new identity will point to the
caller's own identity as its *endorser*.

The resulting identity descriptor, when saved on the distributed ledger,
will receive a unique ID that is algorithmically derived from the public
key and will point to the caller's identity as the endorsing entity.
Moreover, the identity descriptor will point to the caller's identity as
its *endorser*. The unique ID is returned to the caller in case of
success.

**HTTP request**

method: **POST**

path: *(endpoint address)*/v1.0/ids

content type: application/json

body: (a JSON literal with the following structure)

```
{

"pwd": "*(p1)*",

"cid": "*(p2)*",

"pKey": "*(p3)*",

"kType": "*(p4)*",

"ext": *(p5)*

}
```
JSON literal parameters:

**p1**: the password that unlocks the caller's iWallet

**p2**: the identity that the Personal Node must impersonate when calling
the Trusted Node

**p3**: the public key of the new identity, in Base64 format

**p4**: the type of the public key

> Currently, the only public key type supported is ECDSA; so, this is
> the only legal value. This restriction may be removed in the future.

**p5**: the extended info to be attached to the new identity, formatted as a
JSON literal

> This field of the JSON literal is optional: it should be omitted if
> the identity descriptor must be created with an empty ext attribute.

Example of request body, without the ext field for clarity:

```
{

"pwd": "*mysecret*",

"cid": "1K31KZXjcochXpRhjH9g5MxFFTHPi2zEXb",

"pKey":
"BDefACis5x40248TtHxon2r8FRIQJRYeTrR1S7Ub+z5GVh5oXOb4YiRvNF0S8VZtKkdxSwq0Y+ZYnjbnvw7ZRqU=",

"kType": "ECDSA"

}
```

Example of request body, with sample data in the ext field:

```
“pwd”:   “mysecret”,
    “cid”:   “1K31KZXjcochXpRhjH9g5MxFFTHPi2zEXb”,
    “pKey”:  “BDefACis5x40248TtHxon2r8FRIQJRYeTrR1S7Ub+z5GVh5oXOb4YiRvNF0S8VZtKkdxSwq0Y+ZYnjbnvw7ZRqU=”,
    “kType”: “ECDSA”,
    “ext”: {
      "interq-idsc-org": {
        "LEI": "549300TGFLH2QZV80T70",  
        "LegalName": "ACME Corporation",		
        "LegalAddress": {		
          "AddressLine1": "5445 N 27TH St",	
          "City": " Milwaukee",				
          "Region": "WISCONSIN",		
          "Postcode": "53209",	
          "Country": "USA"		
        }
      }
    }
  }
```

Note that in the example above, the sample data section follows an
application-specific data model called "interq-idsc-org". This is basically a
standardized way of providing a pointer to a legal entity.

**HTTP response on success**

status code: **201** Created

body: *(ID of the new identity, calculated from the public key)*

Example of response body (one line of plain text):

`1L2304ZEi0334pPlswWO001qDwpoerazTW`

**List of HTTP responses on failure**

status code:

**409** Conflict

> The new identity cannot be registered due to some conflict with the
> information already stored in the registry. This condition can be due
> to a number of reasons, which *may* be explained as plain text in the
> response body:

-   the public key of the new identity is not unique in the registry;

-   the length of the chain-of-trust would exceed the limit imposed by
    the TF infrastructure.

**413** Payload Too Large

> The request body size exceeds the limit imposed by the TF
> infrastructure, due to the extended information record being too
> large[^2].

**422** Unprocessable Entity

> The request payload do not meet the expectations. This condition can
> be due to a number of reasons, which *may* be explained as plain text
> in the response body:

-   cannot parse the request payload;

-   missing or unrecognized fields;

-   public key type not supported;

-   invalid public key format.

**401** Unauthorized

**500** Internal Server Error

**502** Bad Gateway

**Example**:
`curl -X POST "http://localhost:8180/v1.0/ids" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"cid\": \"1K31KZXjcochXpRhjH9g5MxFFTHPi2zEXb\", \"ext\": \"{org:{\\\"LegalName\\\":\\\"ACME Corporation\\\"}}\", \"kType\": \"ECDSA\", \"pubKey\": \"BDefACis5x40248TtHxon2r8FRIQJRYeTrR1S7Ub+z5GVh5oXOb4YiRvNF0S8VZtKkdxSwq0Y+ZYnjbnvw7ZRqU=\", \"pwd\": \"mysecret\"}"`


### Set status

Allows the caller to change the status of a registered identity.
Identity status can be Active, Suspended or Revoked. Only Active
identities are "valid" for calling any OpenAPI method.

All identities are created in the Active status. The status of an Active
identity can then be set to Suspended or Revoked, while a Suspended
identity can be turned into Active or Revoked. The status of Revoked
entity is final and cannot be changed.

**HTTP request**

method: **PUT**

path: *(endpoint address)*/v1.0/ids/*(p0)*

content type: application/x-www-form-urlencoded

body:

```
pwd=*(p1)*

cid=*(p2)*

op=*(p3)*
```

path parameter:

**p0**: the ID of the target identity

form parameters:

**p1**: the password that unlocks the caller's iWallet

**p2**: the identity that the Personal Node must impersonate when calling
the Trusted Node

**p3**: the status change operation to be performed on the target identity

possible values: A (activate), S (suspend), R (revoke)

**HTTP response on success**

status code: **204** No Content

**List of HTTP responses on failure**

status code:

**400** Bad Request

> The form parameters do not meet the expectations. This condition can
> be due to a number of reasons, which *may* be explained as plain text
> in the response body:

-   unrecognized parameter(s);

-   op parameter missing;

-   op parameter value out of range.

**409** Conflict

> The current status of the target entity is not compatible with the
> requested operation (see the method description for details on the
> "state machine" logic). This case includes operations that would not
> change the current status -- e.g., trying to activate an already
> active identity.

**401** Unauthorized

**500** Internal Server Error

**502** Bad Gateway

**Example**:
`curl -X PUT "http://localhost:8180/v1.0/ids/99N6882kLD7KCPLmUmegf3zxpFgz7kUEUxEy7u3PFkS5" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"cid\": \"51qQzAtfP6HMBK9zrhNykGg2PSQ9EeFucxHY43uc8SX8\", \"op\": \"S\", \"pwd\": \"mysecret\"}"`

### Retrieve an item

Allows the caller to retrieve the descriptor of a registered identity.
The descriptor is a JSON literal with the following structure:

```
{

"id": "..", // ID of the identity

"pKey": "..", // public key value, Base64-encoded

"kType": "..", // public key type

"end": "..", // ID of the endorser (may be empty)

"status": "..", // status of the identity (A / S / R)

"ext": {..} // extended info in JSON format (may be missing)

}
```
Note that the optional ext field may contain any arbitrary JSON literal,
the meaning of which is entirely application-defined. An example of such
structure is given in the documentation of the "register new item"
method -- see §7.5.1.

**HTTP request**

method: **GET**

```
path: *(endpoint address)*/v1.0/ids/*(p0)*?

pwd=*(p1)*

&cid=*(p2)*

```
path parameter:

**p0**: the ID of the identity descriptor to be retrieved

query string parameters:

**p1**: the password that unlocks the caller's iWallet

**p2**: the identity that the Personal Node must impersonate when calling
the Trusted Node

**HTTP response on success**

status code: 200 Ok

body: *(the retrieved identity descriptor, as a JSON literal)*

Example of response body, with missing (empty) ext field for clarity:

```
{

"id": "1L2304ZEi0334pPlswWO001qDwpoerazTW",

"pKey":
"BDefACis5x40248TtHxon2r8FRIQJRYeTrR1S7Ub+z5GVh5oXOb4YiRvNF0S8VZtKkdxSwq0Y+ZYnjbnvw7ZRqU="

"kType": "ECDSA",

"end": "1K31KZXjcochXpRhjH9g5MxFFTHPi2zEXb",

"status": "A"

}
```
Example of response body, with sample data in the ext field:

```
{
    “id”: “1L2304ZEi0334pPlswWO001qDwpoerazTW”,
    “pKey”: “BDefACis5x40248TtHxon2r8FRIQJRYeTrR1S7Ub+z5GVh5oXOb4YiRvNF0S8VZtKkdxSwq0Y+ZYnjbnvw7ZRqU=”
    “kType”: “ECDSA”,
    “end”: “1K31KZXjcochXpRhjH9g5MxFFTHPi2zEXb”,
    “status”: “A”,
    “ext”: {
      "interq-idsc-org": {
        "LEI": "549300TGFLH2QZV80T70",  
        "LegalName": "ACME Corporation",		
        "LegalAddress": {		
          "AddressLine1": "5445 N 27TH St",	
          "City": " Milwaukee",				
          "Region": "WISCONSIN",		
          "Postcode": "53209",	
          "Country": "USA"		
        }
      }
    }
  }
```
**List of HTTP responses on failure**

status code:

**404** Not found

> No identity with the given ID exists in the registry.

**401** Unauthorized

**500** Internal Server Error

**502** Bad Gateway

**Example**:
`curl -X GET "http://localhost:8180/v1.0/ids/1L2304ZEi0334pPlswWO001qDwpoerazTW?cid= 1L2304ZEi0334pPlswWO001qDwpoerazTW&pwd=mysecret" -H "accept: */*"`


#### Notes

(^1): These privileges are granted by means of X509 digital certificates
issued to the user by the TF's Certification Authority, and allow
such user to create "trust roots" -- i.e., identities that have no
endorser and are always at the root of every chain-of-trust (see
§5). The details on how these super-users are created, managed and
authenticated are not documented here.

(^2): The size limit for the extended information field may vary,
depending on the TF implementation.


## License <a name="license"></a>

tenid-service Project source code files are made available under the Apache License, Version 2.0 (Apache-2.0), located in the [LICENSE](LICENSE) file.

